<?php
   include('connection.php');


   

   if (isset($_POST['submit']))
   {
      if(isset($_SESSION['name']))
      {
      $category = strtolower($_POST['category']);
      $level = $_POST['level'];
      $content = $_POST['content'];
      $emphasize = $_POST['emphasize'];
      $choice1 = $_POST['choice1'];
      $choice2 = $_POST['choice2'];
      $choice3 = $_POST['choice3'];
      $choice4 = $_POST['choice4'];
      $correct = $_POST['correct'];


      $category = str_replace("'","\'","$category");
      $content = str_replace("'","\'","$content");;
      $emphasize = str_replace("'","\'","$emphasize");
      $choice1 = str_replace("'","\'","$choice1");
      $choice2 = str_replace("'","\'","$choice2");
      $choice3 = str_replace("'","\'","$choice3");
      $choice4 = str_replace("'","\'","$choice4");
      date_default_timezone_set('Asia/Ho_Chi_Minh');
      $transDate = date('d/m/Y - H:i:s');


      $sql = "INSERT INTO question(
      id,
      category,
      level,
      content,
      emphasize,
      choice1,
      choice2,
      choice3,
      choice4,
      correct,
      contributor,
      time
      ) VALUES (
      null,
      '$category',
      '$level',
      '$content',
      '$emphasize',
      '$choice1',
      '$choice2',
      '$choice3',
      '$choice4',
      '$correct',
      '$_SESSION[username]',
      '$transDate'
      )";
      mysqli_query($conn,$sql);
      //echo "Add question successfully!";
      }
      else
      {
         header('location:index.php?page=sessionExpired');
      }
   }






   ?>
<!Doctype html>
<head>
   <title>Add product page</title>
   <meta charset="utf-8">
</head>
<body>
   <form method="POST" name ="form" action="">
      <fieldset style="width: 50%">
         <legend></legend>
         <h1>Add question form</h1>
         <div class="form-group">
            <label for="productName">Category</label>
            <input type="text" class="form-control" name="category" placeholder="Enter category" required>
         </div>
                    
         <div class="form-group">
            <label for="level">level</label>
            <select class="form-control" id="level" name = "level" required>
               <option>easy</option>
               <option>normal</option>
               <option>hard</option>
            </select>
         </div>

         <div class="form-group">
            <label for="Category">Content</label>
            <input type="text" class="form-control" name="content" placeholder="Enter content" required>
         </div>

         <div class="form-group">
            <label for="Emphasize">Emphasize word</label>
            <input type="text" class="form-control" name="emphasize" placeholder="Enter emphasize word (Separate by comma without no space)">
         </div>

         <div class="form-group">
            <label for="productName">Choice 1</label>
            <input type="text" class="form-control" name="choice1" placeholder="Enter choice 1" required>
         </div>
                    
         <div class="form-group">
            <label for="Price">Choice 2</label>
            <input type="text" class="form-control" name="choice2" placeholder="Enter choice 2" required>
         </div>

         <div class="form-group">
            <label for="Category">Choice 3</label>
            <input type="text" class="form-control" name="choice3" placeholder="Enter choice 3" required>
         </div>

         <div class="form-group">
            <label for="productName">Choice 4</label>
            <input type="text" class="form-control" name="choice4" placeholder="Enter choice 4" required>
         </div>
                    
         <div class="form-group">
            <label for="level">Correct</label>
            <select class="form-control" id="correct" name = "correct" required>
               <option>1</option>
               <option>2</option>
               <option>3</option>
               <option>4</option>
            </select>
         </div>

         <button type="submit" class="btn btn-primary" name ="submit" onclick="myFunction()">Submit</button>
      </fieldset>
   </form>

   <script type="text/javascript">
   function myFunction() {
  alert("Add this question?");
} 
   
</script>
   <br>
</body>
</html>