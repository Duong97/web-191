<!Doctype html>
<head>
   <title>Login page</title>
   <meta charset="utf-8">
</head>
<body>
   <form method="POST" name ="form" action="loginActivity.php">
      <fieldset style="width: 50%">
         <legend>Login form</legend>

         <div class="form-group">
            <label for="username">User Name</label>
            <input type="text" class="form-control" name="username" placeholder="Enter username" require>
         </div>

         <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" placeholder="Enter password" require>
         </div>

         <div class = "form-group">
            <?php
               if (isset($_SESSION['messLogin']))
               {
                  echo $_SESSION['messLogin'];
                  $_SESSION['messLogin'] = "";
               }
            ?>
         </div>
         
         <button type="submit" class="btn btn-primary">Submit</button>
      </fieldset>
   </form>
   <br>
</body>
</html>