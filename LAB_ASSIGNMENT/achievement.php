<?php
  if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
  include('connection.php');
  ?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
  
  <h3>Your achievement</h3>

  <table border="1">
    <?php

      $sql = "select * from score where username = '$_SESSION[username]'";
      $query = mysqli_query($conn,$sql);
      $num_rows = mysqli_num_rows($query);
            
      if ($num_rows != 0) 
      {
        echo 
        "<tr>
        <th style = \"width: 1%\">Score</th>
        <th style = \"width: 1%\">Category</th>
        <th style = \"width: 1%\">Level</th>
        <th style = \"width: 1%\">Time</th>
        </tr>";

        while($row = $query->fetch_assoc()) 
        {
          echo "<tr>";
          echo "<td>".$row["score"]."</td>";
          echo "<td>".$row["category"]."</td>";
          echo "<td>".$row["level"]."</td>";
          echo "<td>".$row["time"]."</td>";
          echo "</tr>";
        }
      }
      ?>
  </table>
  <br>
</body>
</html>