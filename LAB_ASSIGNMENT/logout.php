<?php
   session_start();
   session_destroy(); 
   session_unset(); 
   setcookie('name', -1, time() - 3600);
   header("location: index.php");
   ?>