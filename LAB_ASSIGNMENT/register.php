<!Doctype html>
<head>
   <title>Register page</title>
   <meta charset="utf-8">
</head>
<body>
   <form method="POST" name ="form" action="registerActivity.php">
      <fieldset style="width: 50%">
         <legend>Register form</legend>


         <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Enter name" require>
         </div>

         <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" name="username" placeholder="Enter username" require>
         </div>

         <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" placeholder="Enter email" require>
         </div>


         <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" placeholder="Enter password" require>
         </div>

         <div class="form-group">
            <?php
               if (isset($_SESSION['messRegister']))
               {
                  echo $_SESSION['messRegister'];
                  $_SESSION['messRegister'] = "";
               }
                  
            ?>
         </div>

         <button type="submit" class="btn btn-primary">Submit</button>




         




      </fieldset>
   </form>
   <br>
</body>
</html>