<?php
   session_start();
   setcookie("name", "ok", time()+60*60, "/","", 0);
   if(!isset($_COOKIE["name"]))
   {
         session_unset(); 
         session_destroy(); 
   }
   
   include('connection.php');
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <title>English test page</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--jQuerry-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <!--Bootstrap 4 CSS-->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!--Bootstrap 4 JS-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <link rel="stylesheet" type="text/css" href="stylesheet.css">
   </head>
   <body>
      <div class = "header">
         <div class = "row">
            <div class = "col-sm-2" align="left">
               <img src="Umbrella.png" height="100" width="100">
            </div>
            <div class = "col-sm-10" align="right" style = "font-size: 20px">
               <?php 
                  if (!isset($_SESSION['name'])) 
                  {
                    echo 
                    "<a href=\"index.php?page=login\" style=\"color:white\">Login</a>
                    <a href=\"index.php?page=register\" style=\"color:white\">  Signup</a>
                    ";
                  }
                  else 
                  {
                    echo $_SESSION['name'];
                    echo 
                    "<a href=\"index.php?page=logout\" style=\"color:white\">  Logout</a>
                    ";
                  }
                  ?>
            </div>
         </div>
      </div>
      <nav class="navbar navbar-expand-md bg-dark navbar-dark">
         <ul class="navbar-nav mr-auto  hover">
            <li class="nav-item">
               <a class="nav-link" href="index.php?page=home">Home</a>
            </li>
            <?php
               if (isset($_SESSION['name'])) 
               {
                 echo 
                 "<li class=\"nav-item\"><a class=\"nav-link\" href=\"index.php?page=account\">Account</a></li>";
                 // echo"<li class=\"nav-item\"><a class=\"nav-link\" href=\"index.php?page=add_question\">Add question</a></li>";
               }
               
               ?>
         </ul>
         
         <ul>
            <!-- <form class="form-inline my-2 my-lg-0" action="index.php?page=searchActivity" method="POST">
               <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" id="Search" name = "Search">
               <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
            </form>
            <div id="displaySuggest" style="position: fixed" ></div> -->
         </ul>
         
      </nav>
      <div class= "row">


         <div class = "col-xl-3 col-sm-3" style="background-color: #f1f1f1">

          <div class = "" style="margin-top:5px; margin-bottom:5px;margin-left: 20px">
          <form class="form-inline my-2 my-lg-0" action="index.php?page=searchActivity" method="POST" >
               <input class="form-control mr-sm-2" style = "width: 200px" type="search" placeholder="Search" aria-label="Search" id="Search" name = "Search">
               <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
            </form>
            <div id="displaySuggest" style="position: absolute" ></div>
          </div>




            <ul>
               <?php
                  $sql = "select distinct category from question";
                         
                  $query = mysqli_query($conn,$sql);
                  $num_rows = mysqli_num_rows($query);
                         
                  while($row = $query->fetch_assoc()) 
                  {
                    echo 
                    "<li><a href=\"index.php?page=category&Category=$row[category]\">$row[category]</a></li>";
                  }
                  ?>
            </ul>
         </div>
         <div class = "col-xl-9 col-sm-9">
            <?php
              if (isset($_GET['page'])) 
              {
                $page = $_GET['page'];
                include("$page.php");
              }
                
              else 
              {
                include("home.php");
              }
              ?>
         </div>
      </div>
      <div class="footer text-center">
         <p>Contact: Sample </p>
         <p>Facebook: <a href="#" target = "_blank" style="color:white">Sample</a></p>
      </div>

      <script>  
            $(document).ready(function(){  
                 $('#Search').keyup(function(){  
                      var query = $(this).val();  
                      if(query != '')  
                      {  
                           $.ajax({  
                                url:"suggest.php",  
                                method:"POST",  
                                data:{query:query},  
                                success:function(data)  
                                {  
                                     $('#displaySuggest').fadeIn();  
                                     $('#displaySuggest').html(data);  
                                }  
                           });  
                      }  
                 });  
                 $(document).on('click', 'li', function(){  
                      $('#Search').val($(this).text());  
                      $('#displaySuggest').fadeOut();  
                 });  
            });  
         </script> 
   </body>
</html>