This is assignment of WEB Course at HCMC semester 191, 2019.

Set up database name english_test before using.
Modify parameter of your phpmyadmin in connection.php to connect.

Home page:

<img src="./demo_1.png" width="80%"/>


Start the exam:

<img src="./demo_2.png" width="80%"/>


The result:

<img src="./demo_3.png" width="80%"/>

